package THSR;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import trainsystem.Train;

/**
 * <h1>Table of trains of a date.</h1>
 * <p>
 * TrainTable class has two members,one is the date of the table , the other is
 * a Train array which stored the train list of the date.
 * 
 * @author B05505001
 * @version 1.0
 * @since 2018-06-12
 *
 */
public class TrainTable {
	ArrayList<Train> trainList;
	Calendar date = Calendar.getInstance();

	/**
	 * Constructor of the TrainTable.
	 * 
	 * @param t
	 * @param d
	 * @throws CloneNotSupportedException
	 */
	public TrainTable(ArrayList<Train> t, Calendar d) {
		trainList = t;
		date.setTime(d.getTime());
	}

	/**
	 * Match the TrainTable date.
	 * 
	 * @param departDate
	 *            The date to match.
	 * @return true(match);false(Not match)
	 */
	public boolean trainTableMatch(Calendar departDate) {
		boolean found = false;
		SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
		if (d.format(this.getDate().getTime()).equals(d.format(departDate.getTime())))
			found = true;
		return found;
	}

	/**
	 * Return a copy Train Array from trainTable.
	 * 
	 * @return The copy Train Array from trainTable.
	 * @throws CloneNotSupportedException
	 */
	public ArrayList<Train> getTrainTable() throws CloneNotSupportedException {
		int tableSize = trainList.size();
		ArrayList<Train> temp = new ArrayList<Train>();
		for (int i = 0; i < tableSize; i++) {
			temp.add(trainList.get(i).clone());
		}
		return temp;
	}

	/**
	 * Return the date of the table.
	 * 
	 * @return Date of the table.
	 */
	public Calendar getDate() {
		Calendar temp = Calendar.getInstance();
		temp.setTime(date.getTime());
		return temp;
	}

	/**
	 * Return the info of the train table in form of "||index||Train
	 * number||index||Train number||index||Train number||" in one raw.
	 * 
	 * @return A String object that stores the info of train table.
	 */
	public String toString() {
		String returnS = "||index||Train number||index||Train number||index||Train number";
		int index = 1;
		for (Train e : trainList) {
			if (index % 3 == 1)
				returnS += " ||\n";
			if (index < 10) {
				returnS += "|| " + index++ + ".  ||    " + e.getTrainNo() + "    ";
			} else if (index < 100) {
				returnS += "|| " + index++ + ". ||    " + e.getTrainNo() + "    ";
			} else {
				returnS += "|| " + index++ + ".||    " + e.getTrainNo() + "    ";
			}
		}
		return returnS;

	}

}
