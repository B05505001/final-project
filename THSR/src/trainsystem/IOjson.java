package trainsystem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This Class provides various methods to the json file storing Reservations. It
 * is able to read from and write a json file, add a reservation, or remove a
 * particular reservation.
 * 
 * @author Raymond
 *
 */
public class IOjson {
	public static JSONObject read() {
		JSONObject Jfile = new JSONObject();
		try {
			BufferedReader JSONfile = new BufferedReader(new FileReader("json/test.json"));
			String data = "";
			try {
				data = JSONfile.readLine();
				JSONfile.close();
				Jfile = new JSONObject(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Jfile;
	}

	/**
	 * This method rewrites the json file with the JSONObject provided.
	 * 
	 * @param Jfile
	 *            the JSONObject to be written and saved in the json file.
	 */
	public static void write(JSONObject Jfile) {
		try (FileWriter file = new FileWriter("json/test.json")) {
			file.write(Jfile.toString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a reservation made by booking into the json file.
	 * 
	 * @param J
	 *            The reservation with JSONObject type.
	 */
	public static void add(JSONObject J) {
		JSONObject Jfile = read();
		System.out.println("Pre-order" + Jfile);
		JSONArray Reservations = (JSONArray) Jfile.get("Reservations");
		Reservations.put(J);
		Jfile.put("Reservations", Reservations);
		System.out.println("Post-order" + Jfile);
		write(Jfile);
	}

	/**
	 * Removes a Reservation through user input.
	 */
	public static void remove() {
		String UID;
		int RN;
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Please enter your UID and Reservation Number.\nUID: ");
		UID = keyboard.next();
		System.out.print("Reservation Number: ");
		RN = keyboard.nextInt();
		JSONObject Jfile = new JSONObject();
		JSONArray RJA = (JSONArray) read().get("Reservations");
		JSONArray tempJA = new JSONArray();
		int r = -1;
		boolean found = false;
		for (int i = 0; i < RJA.length(); i++) {
			Jfile = RJA.getJSONObject(i);

			if (!found && Jfile.getString("ID").equals(UID) && Jfile.getInt("OrderNo") == RN) {
				r = i;
				found = true;
			}
		}
		if (found) {
			for (int i = 0; i < RJA.length() - 1; i++) {
				if (i != r) {
					tempJA.put(RJA.get(i));
				}

			}
		} else
			System.out.println("Fatal");
		Jfile.put("Reservations", tempJA);
		write(Jfile);
	}
}
